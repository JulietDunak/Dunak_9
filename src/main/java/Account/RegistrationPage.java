package Account;
import Utils.Click;
import Utils.GenerateEmail;
import Utils.MyWaits;
import Utils.User;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
public class RegistrationPage {
  private final WebDriver driver;
  public RegistrationPage(WebDriver driver) {
    this.driver = driver;
  }
  @FindBy(id = "billinginfo3-form-fullname")
  WebElement namefield;

  @FindBy(xpath = ".//*[@id='billinginfo3_form_countryiso2_chosen']/a")
  WebElement countryfield;

  @FindBy(xpath = ".//*[@placeholder='Start enter country']")
  WebElement countryinput;

  @FindBy(xpath = ".//*[@id='billinginfo3_form_phone_code_chosen']/a/div")
  WebElement dropdownphone;

  @FindBy(xpath =".//*[@id='billinginfo3_form_phone_code_chosen']/div/div/input")
  WebElement phoneinput;

  @FindBy(id = "billinginfo3-form-phone")
  WebElement phonesecondfield;

  @FindBy(id = "billinginfo3-form-postalcode")
  WebElement zipcodefield;

  @FindBy(xpath = ".//*[@id='billinginfo3_form_stateiso2_chosen']/a/div")
  WebElement statefield;

  @FindBy(id = "billinginfo3-form-cityname")
  WebElement cityfield;

  @FindBy(xpath=".//*[contains(@class,'signin-switch-account')]")
  WebElement swithaccount;

  @FindBy(xpath = ".//*[contains(@id,'signin3-form-email')]")
  WebElement emailfield;

  @FindBy(id ="signin3-new-customer")
  WebElement continuebuton;

  @FindBy(id ="billinginfo3-form-submit")
  WebElement submitbutton;

  public void registerAccount(User user) {
//    GenerateEmail generateEmail = new GenerateEmail();
    //Click click=new Click()
    MyWaits myWaits = new MyWaits(driver);
    myWaits.waitEmail();
       if (swithaccount.isDisplayed()) {
      swithaccount.click();
    }
    emailfield.sendKeys(user.generateEmail());
    continuebuton.click();
    myWaits.waitregForm();
    //click.methodClick(namefield);
    namefield.click();
    namefield.sendKeys(user.getName());
    namefield.click();
    countryfield.click();
    countryinput.sendKeys(user.getCountry());
    countryinput.click();
    dropdownphone.click();
    phoneinput.sendKeys(user.getPhone1());
    phoneinput.click();
    phonesecondfield.sendKeys(user.getPhone2());
    phonesecondfield.click();
    zipcodefield.sendKeys(user.getZipcode());
    zipcodefield.click();
    zipcodefield.sendKeys(Keys.TAB);
    if (countryfield.getText().equals("United States")){
    statefield.sendKeys(user.getState());
    statefield.click();
    }
    cityfield.click();
    cityfield.sendKeys(user.getCity());
    cityfield.click();
    submitbutton.click();
}
  public boolean isPaymentMethod(){
    return driver.findElement(By.xpath(".//*[@id='payment-methods-container']/div[1]")).isDisplayed();
  }
}
