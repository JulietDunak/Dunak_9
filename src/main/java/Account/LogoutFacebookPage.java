package Account;

import Utils.MyWaits;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class LogoutFacebookPage {
  public final WebDriver driver;
  public MyWaits waits;
    public LogoutFacebookPage (WebDriver driver) {
        this.driver = driver;
        waits = new MyWaits(driver);
    }

    @FindBy(xpath = (".//*[@id='app-account-menu']/div/button"))
    WebElement menuNav;

   @FindBy(xpath = (".//button[contains(@class,'sign-out')]"))
  //  @FindBy(partialLinkText ="Sign Out")
    WebElement logoutButton;

    public void logoutFacebook() {
        menuNav.click();
        waits.waitClickLogout();
        logoutButton.click();
    }
}