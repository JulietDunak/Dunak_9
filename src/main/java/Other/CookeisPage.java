package Other;
import org.openqa.selenium.WebDriver;

/**
 * Created by it-school on 11.10.2017.
 */
public class CookeisPage {
    private final WebDriver driver;
    public CookeisPage (WebDriver driver) {
        this.driver = driver;
    }
    public String getCookies(String cook) {
       return driver.manage().getCookieNamed(cook).getValue();
    }
}
