package Other;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class CheckPaymentMethodPage {
    private final WebDriver driver;
    public CheckPaymentMethodPage(WebDriver driver) {
        this.driver = driver;
    }
        public boolean isPaymentMethod(){
        return driver.findElement(By.xpath(".//*[@id='payment-methods-container']/div[1]")).isDisplayed();
    }

}
